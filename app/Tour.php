<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tour extends Model
{
    protected $table = 'tour';
    protected $primaryKey = 'id_tour';
    protected $fillable = ['tour_name', 'tour_duration', 'tour_price', 'image', 'description', 'departure', 'id_type_tour', 'hot'];
}
