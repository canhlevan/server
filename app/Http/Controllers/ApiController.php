<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Tour;
use App\TypeTour;
use DB;

class ApiController extends Controller
{
    public function create(Request $request)
    {
        $tours = new Tour();

        $tours->tour_name = $request->input('tour_name');
        $tours->tour_duration = $request->input('tour_duration');
        $tours->tour_price = $request->input('tour_price');
        $tours->description = $request->input('description');
        $tours->image = $request->input('image');
        $tours->departure = $request->input('departure');
        $tours->id_type_tour = $request->input('id_type_tour');
        $tours->hot = $request->input('hot');

        $tours->save();
        return response()->json($tours);
    }

    public function show()
    {
        $tours = Tour::all();
        return response()->json($tours);
    }

    public function showTypeTour()
    {
        $type_tour = TypeTour::all();
        return response()->json($type_tour);
    }

    public function showById($id)
    {
        $tour = Tour::find($id);
        return response()->json($tour);
    }

    public function updateById(Request $request, $id)
    {
        $tours = Tour::find($id);
        $tours->tour_name = $request->input('tour_name');
        $tours->tour_duration = $request->input('tour_duration');
        $tours->tour_price = $request->input('tour_price');
        $tours->description = $request->input('description');
        $tours->image = $request->input('image');
        $tours->departure = $request->input('departure');
        $tours->id_type_tour = $request->input('id_type_tour');
        $tours->hot = $request->input('hot');

        $tours->save();
        return response()->json($tours);
    }

    public function hot($parameter)
    {
            $filter = DB::table('tour')
                    ->where('hot', '=', $parameter)
                    ->get();
            return $filter;
    }

    public function popular($parameter)
    {
            $filter = DB::table('tour')
                    ->where('hot', '=', $parameter)
                    ->get();
            return $filter;
    }
}
