<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class UserController extends Controller
{
    function index()
    {
        return User::all();
    }

    function insert(Request $request)
    {
        $user = new User();
        $user->fullname = $request->fullname;
        $user->email = $request->email;
        $user->password = $request->password;
        $user->address = $request->address;
        $user->phone = $request->phone;

        if($user->save())
        {
            return ['status'=>'data has been inserted'];
        }
    }
}
