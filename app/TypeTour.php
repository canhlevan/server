<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TypeTour extends Model
{
    protected $table = 'type_tour';
    protected $guarded = [''];
}
