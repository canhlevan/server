<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::get('user', 'UserController@index');
Route::post('insert', 'UserController@insert');

Route::post('/tour', 'ApiController@create');

Route::get('/tour', 'ApiController@show');

Route::get('/type-tour', 'ApiController@showTypeTour');



Route::get("/tour/hot/{parameter}", "ApiController@hot");

Route::get('/tour/{id}', 'ApiController@showById');

Route::put('/tourUpdate/{id}', 'ApiController@updateById');