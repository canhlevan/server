<?php

namespace Modules\Admin\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Str;
use Illuminate\Routing\Controller;
use App\Http\Requests\RequestCategory;


class AdminCategoryController extends Controller
{

    public function index()
    {
        $categories = Category::select('id', 'name_type', 'name_type_slug', 'status')->get();
        $viewData = [
            'categories' => $categories
        ];
        return view('admin::category.index', $viewData);
    }

    public function create()
    {
        return view('admin::category.create');
    }

    public function store(RequestCategory $requestCategory)
    {

        
        $this->insertOrUpdate($requestCategory);
        return redirect()->back();
    }

    public function edit($id)
    {
        $category = Category::find($id);
        return view('admin::category.update', compact('category'));
    }

    public function update(RequestCategory $requestCategory, $id)
    {
        $this->insertOrUpdate($requestCategory, $id);
        return redirect()->back();
    }

    public function insertOrUpdate($requestCategory, $id='')
    {
        $code = 1;
        try {
            $category = new Category();
            if($id){
                $category = Category::find($id);
            }
            
            $category->name_type = $requestCategory->name;
            $category->name_type_slug = Str::slug($requestCategory->name, '-');
            if(!$requestCategory->hot){
                $category->status = 0;
            }else{
                $category->status = $requestCategory->hot;
            }
            
            $category->save();
        } catch (\Exception $exception) {
            $code = 0;
            Log::error("[Error insertOrUpdate Category]".$exception->getMessage());
        }

        return $code;
    }
}
