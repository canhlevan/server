@extends('admin::layouts.master')

@section('content')
<nav class="navbar navbar-expand-lg navbar-transparent navbar-absolute fixed-top " id="navigation-example">
    <div class="container-fluid">
        <div class="navbar-wrapper">
            <a class="navbar-brand" href="javascript:void(0)">Quản lý danh mục</a>
        </div>
    </div>
  
</nav>
<div class="container-fluid" style="margin-top: 70px;">
    <nav aria-label="breadcrumb">
            <ol class="breadcrumb" style="background-color: #1a2035;">
                <li class="breadcrumb-item"><a href="{{route('admin.dashboard')}}">Tổng quan</a></li>
                <li class="breadcrumb-item"><a href="{{route('admin.get.list.category')}}">Danh mục</a></li>
                <li class="breadcrumb-item active" aria-current="page">Danh sách</li>
            </ol>
        </nav>
    </div>



<div class="content">
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-12">
              <div class="card">
                <div class="card-header card-header-primary">
                  <h3 class="card-title ">Quản lý loại tour</h3>
                  <a href="{{ route('admin.get.create.category')}}" class="card-category pull-right"> Thêm mới</a>
                </div>
                <div class="card-body">
                  <div class="table-responsive">
                    <table class="table">
                      <thead class=" text-primary">
                        <th>
                          ID
                        </th>
                        <th>
                          LOẠI TOUR
                        </th>
                        <th>
                          SLUG
                        </th>
                        <th>
                          TRẠNG THÁI
                        </th>
                        <th>
                          THAO TÁC
                        </th>
                      </thead>
                      <tbody>
                        @if (isset($categories))
                          @foreach($categories as $category)
                          <tr>
                          <td>
                            {{$category->id}}
                          </td>
                          <td>
                            {{$category->name_type}}
                          </td>
                          <td>
                            {{$category->name_type_slug}}
                          </td>
                          <td>
                            {{$category->status}}
                          </td>
                          <td class="text-primary">
                            <a href="{{route('admin.get.edit.category', $category->id)}}">Edit</a>
                            <a href="">Delete</a>
                          </td>
                        </tr>
                          @endforeach
                        @endif
                        
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
            
          </div>
        </div>
      </div>
@endsection