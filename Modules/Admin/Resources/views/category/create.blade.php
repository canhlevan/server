@extends('admin::layouts.master')

@section('content')
<nav class="navbar navbar-expand-lg navbar-transparent navbar-absolute fixed-top " id="navigation-example">
    <div class="container-fluid">
        <!-- <div class="navbar-wrapper">
            <a class="navbar-brand" href="javascript:void(0)">Quản lý danh mục</a>
        </div> -->
        <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ route('admin.dashboard')}}">Tổng quan</a></li>
            <li class="breadcrumb-item"><a href="{{route('admin.get.list.category')}}">Danh mục</a></li>
            <li class="breadcrumb-item active" aria-current="page">Thêm mới</li>
        </ol>
    </nav>
    </div>
    
</nav>



<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header card-header-primary">
                        <h3 class="card-title ">Thêm mới loại tour</h3>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
                    <div class="col-md-12">
                        <form action="" method="POST">
                            @csrf
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="name">Tên danh mục:</label>
                                    <input type="text" class="form-control" placeholder="Tên loại tour" value="{{ old('name') }}" name="name">
                                    @if($errors->has('name'))
                                        <span class="error-text" style="color: white">
                                            {{$errors->first('name')}}
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">                                       
                                    <input type="checkbox" value="1" name="hot">
                                    <label class="form-check-label" >Nổi bật</label>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <button type="submit" class="btn btn-primary">Lưu</button>
                            </div>

                            
                        </form>
                    </div>
                </div>
    </div>
</div>
@endsection